
import * as actionTypes from '../Actions/ActionTypes';
import convertObjToArray from '../../utils/convertObjToArray';
import data from '../../Data/prComments.json';

const initialState = {
    comments: convertObjToArray(data),
    selectedComment: null,

}


const commentsReducer = (state = initialState, action) => {

    switch(action.type){

        case (actionTypes.SELECT_COMMENT):
            const selectedComment = state.comments.filter(comment => comment.prComments === action.payload);
            return {
                ...state,
                selectedComment: selectedComment
            }

        case (actionTypes.SET_DEFAULT):
            return {
                ...state,
                selectedComment: null
            }


        default:
            return state
    }
}

export default commentsReducer;