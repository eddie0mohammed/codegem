
import * as actionTypes from '../Actions/ActionTypes';


const initialState = {
    searchInput: '',
    sort: 'default',
    topic: 'default',
    type: 'default',
    author: 'default',
}


const filterReducer = (state = initialState, action) => {

    switch(action.type){

        case (actionTypes.INPUT_CHANGE):
            return {
                ...state,
                searchInput: action.payload
            }

        case (actionTypes.SORT):
            return {
                ...state,
                sort: action.payload

            }

        case (actionTypes.TOPIC_CHANGE):
            return {
                ...state,
                topic: action.payload
            }

        case (actionTypes.TYPE_CHANGE):
            return {
                ...state,
                type: action.payload
            }

        case (actionTypes.AUTHOR_CHANGE):
            return {
                ...state,
                author: action.payload
            }

        default:
            return state
    }
}

export default filterReducer;