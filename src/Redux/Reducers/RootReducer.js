

import {combineReducers} from 'redux';

import commentsReducer from './CommentsReducer';
import modalReducer from './ModalReducer';
import filterReducer from './FiltersReducer';


const rootReducer = combineReducers({
    comments: commentsReducer,
    modal: modalReducer,
    filter: filterReducer

});


export default rootReducer;
