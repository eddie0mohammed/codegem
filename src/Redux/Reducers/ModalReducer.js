
import * as actionTypes from '../Actions/ActionTypes';

const initialState = {
    showModal: false,

}

const modalReducer = (state = initialState, action) => {

    switch (action.type){

        case (actionTypes.SHOW_MODAL):
            return {
                ...state,
                showModal: true
            }

        case (actionTypes.HIDE_MODAL):
            return {
                ...state,
                showModal: false
            }

        default:
            return state
    }
}

export default modalReducer;