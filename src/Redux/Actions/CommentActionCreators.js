
import * as actionTypes from './ActionTypes';


export const selectComment = (prCommentId) => {

    return {
        type: actionTypes.SELECT_COMMENT,
        payload: prCommentId
    }
}

export const setDefaultComment = () => {
    return {
        type: actionTypes.SET_DEFAULT
    }
}



