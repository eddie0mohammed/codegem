
export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';


export const SELECT_COMMENT = 'SELECT_COMMENT';
export const SET_DEFAULT = 'SET_DEFAULT';


export const INPUT_CHANGE = 'INPUT_CHANGE';
export const SORT = 'SORT';
export const TOPIC_CHANGE = 'TOPIC_CHANGE';
export const TYPE_CHANGE = 'TYPE_CHANGE';
export const AUTHOR_CHANGE = 'AUTHOR_CHANGE';