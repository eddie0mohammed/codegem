
import * as actionTypes from './ActionTypes';


export const inputChange = (input) => {
    return {
        type: actionTypes.INPUT_CHANGE,
        payload: input
    }
}


export const sort = (type) => {
    return {
        type: actionTypes.SORT,
        payload: type
    }
}

export const handleTopicChange = (topic) => {

    return {
        type: actionTypes.TOPIC_CHANGE,
        payload: topic
    }
}

export const handleTypeChange = (type) => {
    return {
        type: actionTypes.TYPE_CHANGE,
        payload: type
    }
}

export const handleAuthorChange = (author) => {
    return {
        type: actionTypes.AUTHOR_CHANGE,
        payload: author
    }
}