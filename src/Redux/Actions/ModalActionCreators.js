
import * as actionTypes from './ActionTypes';


export const showModal = () => {

    return {
        type: actionTypes.SHOW_MODAL
    }
}


export const hideModal = () => {
    return {
        type: actionTypes.HIDE_MODAL
    }
}