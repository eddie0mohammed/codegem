import React from 'react';

import './App.css';

import Header from './Components/Header/Header';
import List from './Components/List/List';
import DetailedComment from './Components/DetailedComment/DetailedComment';

import Modal from './UI/Modal/Modal';
import Backdrop from './UI/Backdrop/Backdrop';


function App() {

  return (
    <div className="App">

      <Header />
      
      <List />

      <Modal >
        <DetailedComment />
      </Modal>
      <Backdrop />


    </div>
  );
}

export default App;
