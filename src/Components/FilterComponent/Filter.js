import React, {useState} from 'react';

import styles from './Filter.module.css';

import {connect} from 'react-redux';

import {getAllTopics, getAllTypes, getAllCommentAuthors} from '../../utils/helpers';
import * as filterActionCreators from '../../Redux/Actions/FilterActionCreators';


const Filter = (props) => {

    const [input, setInput] = useState('');

    const topicsArr = getAllTopics(props.comments);
    const typesArr = getAllTypes(props.comments);
    const commentAuthorsArr = getAllCommentAuthors(props.comments);
    
    const renderTopicOptions = () => {
        return topicsArr.map(elem => {
            return (
                <option key={elem} value={elem} >{elem}</option>
            )
        })
    }

    const renderTypesOptions = () => {
        return typesArr.map(elem => {
            return (
                <option key={elem} value={elem} >{elem}</option>
            )
        })
    }

    const renderAuthorsOptions = () => {
        return commentAuthorsArr.map(elem => {
            return (
                <option key={elem} value={elem} >{elem}</option>
            )
        })
    }

    const handleInputChange = (e) => {
        setInput(e.target.value);

        props.inputChange(e.target.value);
    }

    const handleSort = (e) => {
        props.sort(e.target.value);
    }
    
    return (
        <div className={styles.container}>

            <div className={styles.row}>

                <input className={styles.input} type="text" placeholder="Search by prComment#" onChange={handleInputChange}/>
                
                <div className={styles.sortContainer}>
                    <label className={styles.label}>Sort:</label>
                    <select className={styles.select} onChange={handleSort}>
                        <option value="default" hidden>Date</option>
                        <option value="default" >Default</option>
                        <option value="asc" >Ascending</option>
                        <option value="desc" >Decending</option>
                    </select>
                </div>
            </div>

            <div className="row">
                <label className={styles.label}>Filters:</label>
                
                <select className={styles.select} onChange={(e) => props.handleTopicChange(e.target.value)}>
                    <option value="default" hidden>Topic</option>
                    <option value="default" >All</option>
                    {renderTopicOptions()}
                </select>

                <select className={styles.select} onChange={(e) => props.handleTypeChange(e.target.value)}>
                    <option value="default" hidden>Type</option>
                    <option value="default" >All</option>
                    {renderTypesOptions()}
                </select>

                <select className={styles.select} onChange={(e) => props.handleAuthorChange(e.target.value)}>
                    <option value="default" hidden>Author</option>
                    <option value="default" >All</option>
                    {renderAuthorsOptions()}
                </select>
            </div>
            
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        comments: state.comments.comments
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        inputChange: (input) => dispatch(filterActionCreators.inputChange(input)),
        sort: (type) => dispatch(filterActionCreators.sort(type)),
        handleTopicChange: (topic) => dispatch(filterActionCreators.handleTopicChange(topic)),
        handleTypeChange: (type) => dispatch(filterActionCreators.handleTypeChange(type)),
        handleAuthorChange: (author) => dispatch(filterActionCreators.handleAuthorChange(author)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filter);