import React from 'react'

import styles from './DetailedComment.module.css';

import {connect} from 'react-redux';

import * as modalActionCreators from '../../Redux/Actions/ModalActionCreators';
import * as commentActionCreators from '../../Redux/Actions/CommentActionCreators';

const DetailedComment = (props) => {

    const {prComments} = props.selectedComment[0];

    const {
        prNumber, 
        commentAuthor, 
        prAuthor, 
        prId, 
        commentCreatedAt, 
        commentTopic, 
        commentType
    } = props.selectedComment[0][Number(prComments)];


    const handleClick = () => {
        props.hideModal();
        props.setDefaultComment();
    }

    return (
        <div className={styles.container}>

            <div 
                className={styles.close}
                onClick={handleClick}
            >
                &times;
            </div>
            
            <div className={styles.row}>
                <p className={styles.details}>prComment#: <span className={styles.content}>{prComments}</span></p>
            </div>

            <h2 className={styles.label}>Details</h2>

            <div className={styles.row}>
                <p className={styles.details}>Date Created: <span className={styles.content}>{commentCreatedAt}</span></p>
                <p className={styles.details}>Comment Author: <span className={styles.content}>{commentAuthor}</span></p>
            </div>
            
            <div className={styles.row}>
                <p className={styles.details}>commentTopic: <span className={styles.content}>{commentTopic}</span></p>
                <p className={styles.details}>commentType: <span className={styles.content}>{commentType}</span></p>
            </div>
            
            <div className={styles.row}>
                <p className={styles.details}>prId: <span className={styles.content}>{prId}</span></p>
            </div>

            <div className={styles.row}>
                <p className={styles.details}>prNumber: <span className={styles.content}>{prNumber}</span></p>
                <p className={styles.details}>prAuthor: <span className={styles.content}>{prAuthor}</span></p>
            </div>
            
        
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        selectedComment: state.comments.selectedComment
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        hideModal: () => dispatch(modalActionCreators.hideModal()),
        setDefaultComment: () => dispatch(commentActionCreators.setDefaultComment()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailedComment);