import React from 'react';

import styles from './List.module.css';

import ListRow from '../ListRow/ListRow';
import FilterComponent from '../FilterComponent/Filter';

import {connect} from 'react-redux';


const List = (props) => {

    const applyFilters = (comments) => {
        //search
        if (props.searchInput){
            comments = comments.filter(elem => elem.prComments.startsWith(props.searchInput))
        }
        //sort
        if (props.sort === 'asc'){
            comments = comments.sort((a, b) => new Date(a[Number(a.prComments)].commentCreatedAt) - new Date(b[Number(b.prComments)].commentCreatedAt));
        }else if (props.sort === 'desc'){
            comments = comments.sort((a, b) => new Date(b[Number(b.prComments)].commentCreatedAt) - new Date(a[Number(a.prComments)].commentCreatedAt));
        }
        //topic
        if (props.topic !== 'default'){
            comments = comments.filter(elem => elem[Number(elem.prComments)].commentTopic === props.topic);
        }
        //type
        if (props.type !== 'default'){
            comments = comments.filter(elem => elem[Number(elem.prComments)].commentType === props.type);
        }
        //author
        if (props.author !== 'default'){
            comments = comments.filter(elem => elem[Number(elem.prComments)].commentAuthor === props.author);
        }
        


        return comments;
    }

    const renderListItems = () => {
 
        return applyFilters(props.comments).map(comment => {
            return (
                <ListRow  key={comment.prComments} comment={comment[Number(comment.prComments)]}/>
            ) 
        })
    }

    
    return (
        <div className={styles.container}>

            <FilterComponent />

            <div className={styles.innerContainer}>

                <div className={styles.row}>
                    <div className={styles.cell}>
                        <p className={styles.heading}>prComment#</p>
                    </div>
                    <div className={styles.cell}>
                        <p className={styles.heading}>Date</p>
                    </div>
                    <div className={styles.cell}>
                        <p className={styles.heading}>Topic</p>
                    </div>
                    <div className={styles.cell}>
                        <p className={styles.heading}>Type</p>
                    </div>
                    <div className={styles.cell}>
                        <p className={styles.heading}>Comment Author</p>
                    </div>
                    <div className={styles.cell}>
                    <p className={`${styles.heading1} ${styles.heading}`}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                    </div>                    
                </div> 


                {props.comments && props.comments.length > 0 && 
                    renderListItems()
                }
                
                
                

            </div>
            
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        comments: state.comments.comments,
        searchInput: state.filter.searchInput,
        sort: state.filter.sort,
        topic: state.filter.topic,
        type: state.filter.type,
        author: state.filter.author
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(List);