
import React from 'react';

import styles from './Header.module.css';

const Header = (props) => {
    return (
        <div className={styles.container}>

            <div className={styles.logo}>Code<span className={styles.gem}>Gem</span></div>
            
        </div>
    )
}

export default Header;