import React from 'react'

import styles from './ListRow.module.css';

import {connect} from 'react-redux';
import * as modalActionCreators from '../../Redux/Actions/ModalActionCreators';
import * as commentActionCreators from '../../Redux/Actions/CommentActionCreators';


const ListRow = (props) => {

    const {
        prComments, 
        commentAuthor, 
        commentCreatedAt, 
        commentTopic, 
        commentType
    } = props.comment;

    const handleViewClick = () => {
        props.showModal();

        props.selectComment(prComments);

    }

    return (
        
        <div className={styles.row}>

            <div className={styles.cell}>
                <p className={styles.content}>{prComments}</p>
            </div>
            <div className={styles.cell}>
                <p className={styles.content}>{commentCreatedAt}</p>
            </div>
            <div className={styles.cell}>
                <p className={styles.content}>{commentTopic}</p>
            </div>
            <div className={styles.cell}>
                <p className={styles.content}>{commentType}</p>
            </div>
            <div className={styles.cell}>
                <p className={styles.content}>{commentAuthor}</p>
            </div>

            <div className={styles.cell}>  
                <div className={`${styles.content} ${styles.link}`} onClick={handleViewClick}>View</div>
            </div>
        </div>
            
    )
}

const mapStateToProps = (state) => {
    return {

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        showModal: () => dispatch(modalActionCreators.showModal()),
        selectComment: (prCommentId) => dispatch(commentActionCreators.selectComment(prCommentId)), 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListRow);