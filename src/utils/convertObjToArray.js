
const convertObjToArray = (obj) => {
    let arr = [];

    for (let key in obj['prComments']){
        let temp = {}
        temp[key] = obj['prComments'][key];
        temp.prComments = key;
        temp[key].prComments = key;
        arr.push(temp);
    }

    return arr;
}

export default convertObjToArray;