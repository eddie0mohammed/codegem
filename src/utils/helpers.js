
export const getAllTopics = (comments) => {

    let obj = {};
    
    comments.forEach(elem => {
        if (obj.hasOwnProperty(elem[Number(elem.prComments)].commentTopic)){
            obj[elem[Number(elem.prComments)].commentTopic]++;
        }else{
            obj[elem[Number(elem.prComments)].commentTopic] = 1;
        }
    });
    
    let newArr = [];
    for (let key in obj){
        newArr.push(key);
    }
    return newArr;
}


export const getAllTypes = (comments) => {

    let obj = {};
    
    comments.forEach(elem => {
        if (obj.hasOwnProperty(elem[Number(elem.prComments)].commentType)){
            obj[elem[Number(elem.prComments)].commentType]++;
        }else{
            obj[elem[Number(elem.prComments)].commentType] = 1;
        }
    });
    
    let newArr = [];
    for (let key in obj){
        newArr.push(key);
    }
    return newArr;
}

export const getAllCommentAuthors = (comments) => {

    let obj = {};
    
    comments.forEach(elem => {
        if (obj.hasOwnProperty(elem[Number(elem.prComments)].commentAuthor)){
            obj[elem[Number(elem.prComments)].commentAuthor]++;
        }else{
            obj[elem[Number(elem.prComments)].commentAuthor] = 1;
        }
    });
    
    let newArr = [];
    for (let key in obj){
        newArr.push(key);
    }
    return newArr;
}