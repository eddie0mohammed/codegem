import React from 'react'

import styles from './Modal.module.css';

import {connect} from 'react-redux';

const Modal = (props) => {

    return (
        <div className={styles.modal} style={{display: `${props.showModal ? 'block' : 'none'}`}}>

            {props.selectedComment && props.children}
            
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        showModal: state.modal.showModal,
        selectedComment: state.comments.selectedComment
    }
}


export default connect(mapStateToProps)(Modal);
