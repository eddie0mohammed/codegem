import React from 'react';

import styles from './Backdrop.module.css';

import {connect} from 'react-redux';
import * as modalActionCreators from '../../Redux/Actions/ModalActionCreators';
import * as commentActionCreators from '../../Redux/Actions/CommentActionCreators';

const Backdrop = (props) => {

    const handleClick = () => {
        props.hideModal();
        props.setDefaultComment();
    }

    return (
        <div 
            className={styles.backdrop} 
            style={{display: `${props.showModal ? 'block' : 'none'} `}}
            onClick={handleClick}
        >
            
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        showModal: state.modal.showModal
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        hideModal: () => dispatch(modalActionCreators.hideModal()),
        setDefaultComment: () => dispatch(commentActionCreators.setDefaultComment())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Backdrop);